import "reflect-metadata";
import { DataSource, DataSourceOptions } from "typeorm";

export const startDbConnection = async (databaseConfig: DataSourceOptions): Promise<DataSource> => {
    try {
        const dataSource = new DataSource(databaseConfig)

        await dataSource.initialize()

        console.log(`Connection to database success!`);

        return dataSource
    } catch (e) {
        throw e
    }
}
