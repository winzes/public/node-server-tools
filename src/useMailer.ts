import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

export const useMailer = (options: SMTPTransport | SMTPTransport.Options) => {
    const transporter = nodemailer.createTransport(options);

    const sendMail = async (options: Mail.Options) => {
        return new Promise((res, rej) => {
            transporter.sendMail(options, function (error, info) {
                if (error) {
                    rej(error);
                } else {
                    res(info)
                }
            });
        })
    }

    return {
        sendMail
    }
}
