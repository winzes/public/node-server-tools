import jwt, { TokenExpiredError } from 'jsonwebtoken';
import {
    Decoded,
    JwtToken,
    TokenExpireFunction,
    TokenOptions
} from './jwt-manager.types';

const defaultOptions: TokenOptions = {
    tokenSecret: 'default_secret',
    expiteInMinutes: 60,
    refreshExpireInMinutes: 60
}

export const generateToken = <T>(data: T, options: TokenOptions = defaultOptions): JwtToken => {
    const { expiteInMinutes, tokenSecret } = options;

    const token: string = jwt.sign({
        data
    }, tokenSecret, { expiresIn: expiteInMinutes * 60 });

    const timestamp = new Date().getTime() + expiteInMinutes * 60 * 1000;

    return {
        token,
        expireDate: new Date(timestamp).toISOString()
    };
};

export const validateToken = <T>(
    token: string,
    options: TokenOptions = defaultOptions,
    tokenExpireCallback?: TokenExpireFunction<T>
): T => {
    const { tokenSecret, refreshExpireInMinutes } = options;

    try {
        const decoded = jwt.verify(token, tokenSecret);

        return (decoded as Decoded<T>).data
    } catch (error) {
        if (tokenExpireCallback && error instanceof TokenExpiredError) {
            const { data, exp } = jwt.verify(
                token, tokenSecret, { ignoreExpiration: true }
            ) as Decoded<T>;

            const isRefreshTokenExpired = !!refreshExpireInMinutes &&
                (Date.now() / 1000) > exp + (refreshExpireInMinutes * 60);

            return tokenExpireCallback(data, exp, isRefreshTokenExpired)
        }

        return null;
    }
};
