import hash from 'object-hash'

export const getObjectHash = (obj: object): string => {
    return hash(obj)
}

export const jsonTryParse = <T>(jsonString: string): T | null => {
    try {
        return JSON.parse(jsonString)
    } catch {
        return null;
    }
}

export const jsonTryParseOrString = <T>(jsonString: string): T | string => {
    try {
        return JSON.parse(jsonString)
    } catch {
        return jsonString;
    }
}
