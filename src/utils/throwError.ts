export class CustomError extends Error {
    constructor(message: string, name: string) {
        super(message);
        this.name = name;
    }
}

export const throwCustomError = (message: string, name: string = 'CustomError') => {
    throw new CustomError(message, name)
}

export const throwCustomErrorWithStatusCode = (message: string, statusCode: number, name: string = 'CustomError') => {
    const error = {
        message,
        statusCode
    }
    throw new CustomError(JSON.stringify(error), name)
}
