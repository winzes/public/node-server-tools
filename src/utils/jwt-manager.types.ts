export type TokenOptions = Readonly<{
    tokenSecret: string,
    expiteInMinutes?: number
    refreshExpireInMinutes?: number
}>

export type TokenExpireFunction<T> = (data: T, exp: number, isRefreshTokenExpired: boolean) => T;

export type Decoded<T> = Readonly<{
    data: T,
    exp: number
}>

export type JwtToken = Readonly<{
    token: string;
    expireDate: string;
}>;
