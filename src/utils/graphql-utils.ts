import EventEmitter from "events";
import { FieldNode, GraphQLResolveInfo, SelectionSetNode } from "graphql";

export const getHeaderValueFromContext = (context: any, headerName: string): string => {
    return context?.reply?.request?.headers[headerName] as string
}

const getSelectionsRecursive = (
    selectionSet: SelectionSetNode,
    selectionsOutput: string[],
    parentPropName?: string
) => {
    const selections = selectionSet.selections as FieldNode[];

    selections.map(({ name, selectionSet }) => {
        const fieldName = name.value

        if (fieldName === '__typename') {
            return
        }

        if (!selectionSet && fieldName) {
            const generateName = parentPropName ? `${parentPropName}.${fieldName}` : fieldName
            selectionsOutput.push(generateName)

            return
        }

        getSelectionsRecursive(selectionSet, selectionsOutput, parentPropName ? `${parentPropName}.${fieldName}` : fieldName)
    })
}

export const getQueryFields = (info: GraphQLResolveInfo): string[] => {
    const select: string[] = []
    const fieldNodes: FieldNode = info?.fieldNodes?.[0]
    const selectionSet: SelectionSetNode = fieldNodes && fieldNodes.selectionSet

    selectionSet && getSelectionsRecursive(selectionSet, select)

    return select
}

export class CustomPubSub {
    emitter: EventEmitter

    constructor() {
        this.emitter = new EventEmitter()
    }

    async subscribe(topic: string, queue: any) {
        const listener = (value: any) => {
            queue.push(value)
        }

        const close = () => {
            this.emitter.removeListener(topic, listener)
        }

        this.emitter.on(topic, listener)
        queue.close = close
    }

    publish<TResult = any>(event: { topic: string; payload: TResult }, callback: () => void) {
        this.emitter.emit(event.topic, event.payload)
        callback()
    }
}
