type CacheItem<T> = {
    item: T,
    expiry?: number
}

export const useLocalCache = () => {
    const inMemoryCache = {}

    const set = async <T>(
        key: string,
        item: T,
        seconds?: number
    ): Promise<void> => {
        const cacheItem: CacheItem<T> = {
            item,
            expiry: seconds ? new Date().getTime() + (seconds * 1000) : undefined
        }

        inMemoryCache[key] = cacheItem
    }

    const get = async <T>(key: string): Promise<T | null> => {
        const cacheItem = inMemoryCache[key] as CacheItem<T>

        if (!cacheItem) {
            return null
        }

        if (cacheItem.expiry && new Date().getTime() > cacheItem.expiry) {
            inMemoryCache[key] = undefined
            return null;
        }

        return cacheItem.item
    }

    const remove = async (keys: string[]): Promise<number> => {
        keys.forEach(key => {
            inMemoryCache[key] = undefined
        })
        return 1
    }

    const removeByPrefix = async (startWithKey: string): Promise<void> => {
        const keysToRemove = Object.keys(inMemoryCache).filter(key => key.startsWith(startWithKey))
        remove(keysToRemove)
    }

    const executeWithCache = async <T>(
        key: string,
        action: Function,
        seconds?: number
    ): Promise<T> => {
        const itemFromCache = await get<T>(key);

        if (itemFromCache) {
            return itemFromCache
        }

        const serviceResult = await action() as T

        await set(key, serviceResult, seconds)

        return serviceResult
    }

    return {
        get,
        set,
        remove,
        removeByPrefix,
        executeWithCache
    }
}
