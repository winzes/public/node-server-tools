import crypto from 'crypto';

export const encrypt = (dataString: string): string => {
    return crypto.createHash('sha256')
        .update(dataString)
        .digest('base64');
};
