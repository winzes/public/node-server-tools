type BeforeFunctionCallback = <T>(funtionName: string, args: T) => void;
type AfterFunctionCallback = <T, TT>(funtionName: string, args: T, result: TT) => void;

export const useProxy = <T extends object>(
    obj: T,
    beforeFunctionCallback: BeforeFunctionCallback,
    afterFunctionCallback: AfterFunctionCallback
) => {
    const handler = {
        get(target: object, propKey: string | number | symbol, receiver: any) {
            const targetValue = Reflect.get(target, propKey, receiver)
            if (typeof targetValue === 'function') {
                return (...args: any) => {
                    const functionName = propKey.toString()

                    beforeFunctionCallback(functionName, args)

                    const result = targetValue.apply(this, args)

                    afterFunctionCallback(functionName, args, result)

                    return result
                }
            } else {
                return targetValue
            }
        }
    }
    return new Proxy(obj, handler)
}
