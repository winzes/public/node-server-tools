const TOKEN_STORE = new Map<number, string[]>()

export const setTokenToStore = (userId: number, token: string): void => {
    TOKEN_STORE.get(userId)?.push(token) || TOKEN_STORE.set(userId, [token])
}

export const isValidTokenInStore = (userId: number, token: string): boolean =>
    TOKEN_STORE.get(userId)?.some(userToken => userToken === token)

export const removeTokenForUser = (userId: number, token: string): void => {
    const userTokens = TOKEN_STORE.get(userId)
    if (!userTokens) {
        return
    }

    userTokens.splice(userTokens.indexOf(token), 1)

    if (!userTokens.length) {
        TOKEN_STORE.delete(userId)
    }
}

export const removeAllTokensForUser = (userId: number): void => {
    TOKEN_STORE.delete(userId)
}