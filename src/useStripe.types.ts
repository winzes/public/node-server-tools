import Stripe from "stripe";

export type PaymentIntentOptions<T> = Stripe.PaymentIntentCreateParams & {
    metadata: T
}

export type StripePaymentResult<T> = Stripe.Response<Stripe.PaymentIntent> & {
    metadata: T
}

export type StripeEventData<T> = Stripe.Event.Data & {
    object: {
        metadata: T,
        id: string
        object: string
        amount: number
        currency: string
        charges: {
            data: {
                id: string
                amount: number
                currency: string
                paid: boolean
            }[]
        }
    }
}

export type StripeWebhookHandler<T = any> = (data: StripeEventData<T>, type: string) => void | Promise<void>;

export type StripeWebHookOptions = {
    port: number;
    route?: string;
    secret: string;
    handler: StripeWebhookHandler;
}
