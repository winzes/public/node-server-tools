import gql from "graphql-tag";

export default gql`
    scalar Upload

    "Date in YYYY-MM-DD format"
    scalar Date

    "DateTime in RFC3339 (yyyy-MM-dd'T'HH:mm:ss'Z') format"
    scalar DateTime

    type Query {
        _empty: String
    }

    type Mutation {
        _empty: String
    }

    type Subscription {
        _empty: String
    }
`;