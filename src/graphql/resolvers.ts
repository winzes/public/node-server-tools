import GraphQLUpload from 'graphql-upload/GraphQLUpload.js'

export default {
    Query: {
        _empty: () => null,
    },
    Mutation: {
        _empty: () => null,
    },
    Subscription: {
        _empty: () => null,
    },
    Upload: GraphQLUpload,
}
