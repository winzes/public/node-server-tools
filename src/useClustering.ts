import cluster from 'cluster';
import { cpus } from 'os';
import { ClusteringConfig } from './types';

const setupWorkerProcesses = (options: ClusteringConfig = {}) => {
    const { maxInstancesCount } = options;

    // to read number of cores on system
    const numCores = maxInstancesCount || cpus().length;
    console.log('Master cluster setting up ' + numCores + ' workers');

    // iterate on number of cores need to be utilized by an application
    // current example will utilize all of them
    for (let i = 0; i < numCores; i++) {
        // creating workers and pushing reference in an array
        // these references can be used to receive messages from workers
        cluster.fork()
    }

    // process is clustered on a core and process id is assigned
    cluster.on('online', function (worker) {
        console.log('Worker ' + worker.process.pid + ' is listening');
    });

    // if any of the worker process dies then start a new one by simply forking another one
    cluster.on('exit', function (worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        console.log('Starting a new worker');
        cluster.fork();
    });
};

export const withClustering = (startServerFunc: Function, options?: ClusteringConfig) => {
    // if it is a master process then call setting up worker process
    if (cluster.isMaster) {
        setupWorkerProcesses(options);
    } else {
        // to setup server configurations and share port address for incoming requests
        startServerFunc();
    }
};