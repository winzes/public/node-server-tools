import Stripe from 'stripe';
import fastify from 'fastify';
import cors from '@fastify/cors';
import {
    StripeEventData,
    PaymentIntentOptions,
    StripePaymentResult,
    StripeWebHookOptions,
} from './useStripe.types';

export const useStripe = (apiKey: string) => {
    const stripe = new Stripe(apiKey, {
        apiVersion: '2022-08-01'
    })

    const createPaymentIntent = async <T = any>(
        paymentOptions: PaymentIntentOptions<T>
    ): Promise<StripePaymentResult<T>> => {
        return await stripe.paymentIntents.create(paymentOptions) as StripePaymentResult<T>;
    }

    const getPaymentIntent = async <T>(id: string): Promise<StripePaymentResult<T>> => {
        return await stripe.paymentIntents.capture(id) as StripePaymentResult<T>;
    }

    const addWebHook = (options: StripeWebHookOptions): void => {
        const { port, route, secret, handler } = options

        const webHookRoute = route || `/stripe/webhook`

        const server = fastify();

        server.register(cors);

        server.addContentTypeParser("application/json", { parseAs: "string" },
            (req, body, done) => done(null, body)
        );

        server.post(webHookRoute, async (req, res) => {
            const signature = req.headers['stripe-signature'];

            try {
                const event = stripe.webhooks.constructEvent(
                    (req.body as string),
                    signature,
                    secret,
                );

                const data = (event.data || (req.body as any).data) as StripeEventData<any>;
                const eventType = event.type || (req.body as any).type as string;

                await handler(data, eventType)
            } catch (err) {
                console.log(err.message);
                return res.status(400).send();
            }

            res.status(200).send();
        })

        server.listen({ port, host: '0.0.0.0' }, (err) => {
            if (err) throw err

            console.log(`Stripe WebHook listen on http://127.0.0.1:${port}${webHookRoute}`)
        })
    }

    const createCheckoutSession = async (options: Stripe.Checkout.SessionCreateParams) => {
        return await stripe.checkout.sessions.create(options);
    }

    return {
        instance: stripe,
        createPaymentIntent,
        getPaymentIntent,
        addWebHook,
        createCheckoutSession
    }
}
