import { FastifyInstance, FastifyReply, FastifyRequest } from "fastify"
import { IncomingMessage } from "http"

export type RootServerConfig = Readonly<{
    host: string;
    port: number;
    staticFolderPath?: string;
    staticRoutePrefix?: string;
    injectCustomRoutes?: (server: FastifyInstance) => void
    contextBuilder?: (req: FastifyRequest, res: FastifyReply) => Record<string, any> | Promise<Record<string, any>>;
}>

type GraphqlSubscriptionsConfig = Readonly<{
    contextBuilder?: (req: FastifyRequest) => Record<string, any> | Promise<Record<string, any>>;
    verifyClient?: (req: IncomingMessage) => boolean | Promise<boolean>;
    onConnect?: (type: "connection_init", payload: any) => Record<string, any> | Promise<Record<string, any>>;
    redisConfig?: {
        host?: string,
        port?: number,
        username?: string,
        password?: string
        connectionString?: string
    }
}>

export type GraphqlServerConfig = RootServerConfig & Readonly<{
    graphqlRoute?: string;
    useAltairPlayground?: boolean;
    playgroundRoute?: string;
    disablePlayground?: boolean;
    graphqlSubscriptions?: GraphqlSubscriptionsConfig;
}>

export type ClusteringConfig = Readonly<{
    maxInstancesCount?: number;
}>
