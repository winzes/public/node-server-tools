import fastify, {
  FastifyInstance,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import mercurius, { IResolvers, MercuriusContext } from "mercurius";
import MercuriusGQLUpload from "mercurius-upload";
import AltairFastify from "altair-fastify-plugin";
import cors from "@fastify/cors";
import fastifyStatic from "@fastify/static";
import fastifyWebsocket, { SocketStream } from "@fastify/websocket";
import { DocumentNode, GraphQLError, GraphQLResolveInfo } from "graphql";
import { GraphqlServerConfig } from "./types";
import { ReadStream } from "fs";
import redis from "mqemitter-redis";
import { jsonTryParseOrString } from "./utils";

export type GraphQLContext = MercuriusContext;

export type GraphQLSubscriptionsContext = MercuriusContext & {
  _connectionInit: any;
};

export type GraphQLInfo = GraphQLResolveInfo;

export type TypeDefs = DocumentNode[];

export type Resolvers = IResolvers<any, any> | Array<IResolvers<any, any>>;

export type GraphQLFile = {
  createReadStream: () => ReadStream;
  filename: string;
};

const DEFAULT_VALUES = {
  PLAYGROUND_URL: "/playground",
  GQL_PATH: "/graphql",
  WITH_ALTAIR: true,
};

const addGraphQLServer = async (
  app: FastifyInstance,
  typeDefs: TypeDefs,
  resolvers: Resolvers,
  config: GraphqlServerConfig
) => {
  const {
    useAltairPlayground = DEFAULT_VALUES.WITH_ALTAIR,
    playgroundRoute = DEFAULT_VALUES.PLAYGROUND_URL,
    graphqlRoute = DEFAULT_VALUES.GQL_PATH,
    disablePlayground,
    graphqlSubscriptions,
    contextBuilder,
  } = config;

  app.register(fastifyWebsocket, {
    options: {
      maxPayload: 1048576,
    },
  });

  app.register(MercuriusGQLUpload);

  const { makeExecutableSchema } = await import("@graphql-tools/schema");

  const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
    resolverValidationOptions: {
      requireResolversForArgs: "error",
    },
  });

  let emitter = undefined;

  if (graphqlSubscriptions?.redisConfig) {
    emitter = redis({
      port: graphqlSubscriptions.redisConfig.port,
      host: graphqlSubscriptions.redisConfig.host,
      username: graphqlSubscriptions.redisConfig.username,
      password: graphqlSubscriptions.redisConfig.password,
      connectionString: graphqlSubscriptions.redisConfig.connectionString,
    });
  }

  app.addContentTypeParser(
    "application/json",
    { parseAs: "string" },
    (req, body, done) => {
      if (req.url.includes("stripe")) {
        return done(null, body);
      }
      return done(null, jsonTryParseOrString(body as string));
    }
  );

  app.register(mercurius, {
    schema,
    jit: 1,
    graphiql: disablePlayground
      ? false
      : useAltairPlayground
      ? false
      : "graphiql",
    ide: false,
    path: graphqlRoute,
    context: contextBuilder
      ? async (request: FastifyRequest, reply: FastifyReply) => {
          return await contextBuilder(request, reply);
        }
      : undefined,
    errorFormatter: (err, ctx) => {
      const response = mercurius.defaultErrorFormatter(err, ctx);
      const error = err.errors?.[0];

      if (error && error.message.includes("statusCode")) {
        const errorObj = JSON.parse(error.message);
        response.statusCode = errorObj.statusCode || 500;
        (response.response.errors[0] as GraphQLError).message =
          errorObj.message || error.message;
      }

      return response;
    },
    subscription: graphqlSubscriptions && {
      emitter,
      onConnect: graphqlSubscriptions.onConnect
        ? ({ type, payload }) => {
            return {
              payload,
            };
          }
        : undefined,
      context: graphqlSubscriptions.contextBuilder
        ? async (_connection: SocketStream, request: FastifyRequest) => {
            const { contextBuilder } = graphqlSubscriptions;

            return await contextBuilder(request);
          }
        : undefined,
      verifyClient: graphqlSubscriptions.verifyClient
        ? async ({ req }, next) => {
            const { verifyClient } = graphqlSubscriptions;

            const isVerified = await verifyClient(req);

            next(isVerified);
          }
        : undefined,
    },
  });

  useAltairPlayground &&
    !disablePlayground &&
    app.register(AltairFastify, {
      path: playgroundRoute,
      baseURL: `${playgroundRoute}/`,
      endpointURL: graphqlRoute,
    });
};

export const startGQLServer = async (
  typeDefs: TypeDefs,
  resolvers: Resolvers,
  config: GraphqlServerConfig
) => {
  const {
    host,
    port,
    playgroundRoute = DEFAULT_VALUES.PLAYGROUND_URL,
    graphqlRoute = DEFAULT_VALUES.GQL_PATH,
    staticFolderPath,
    staticRoutePrefix,
    disablePlayground,
    injectCustomRoutes,
  } = config;

  const app = fastify();
  app.register(cors);

  await addGraphQLServer(app, typeDefs, resolvers, config);

  staticFolderPath &&
    app.register(fastifyStatic, {
      root: staticFolderPath,
      prefix: staticRoutePrefix,
      decorateReply: false,
    });

  injectCustomRoutes?.(app);

  try {
    await app.listen({ host: "0.0.0.0", port });
    console.log(
      `GraphQL Server Available at http://${host}:${port}${graphqlRoute}`
    );
    !disablePlayground &&
      console.log(
        `Playground UI Available at http://${host}:${port}${playgroundRoute}`
      );
  } catch (error) {
    console.error("Error starting GQL Server: ", error.message);
  }
};
