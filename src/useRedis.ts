import { createClient } from 'redis';
import {
    RedisConfig,
    RedisClientPubSub,
    RedisClient
} from './useRedis.types';
import { jsonTryParse, useLocalCache } from './utils';

const createRedisClient = async (config: RedisConfig) => {
    const redis = createClient({
        socket: {
            host: config.host,
            port: config.port,
            passphrase: config.password,
            family: config.ipVersion
        },
        database: config.dbIndex
    });

    redis.on('error', (err) => console.log('Redis Client Error', err));

    await redis.connect();

    return redis;
}

export const useRedisStore = async (config?: RedisConfig): Promise<RedisClient> => {
    if (!config) {
        return useLocalCache()
    }

    const redis = await createRedisClient(config);

    const set = async <T>(key: string, value: T, seconds?: number): Promise<void> => {
        if (seconds) {
            await redis.setEx(key, seconds, JSON.stringify(value));
            return
        }
        await redis.set(key, JSON.stringify(value));
    }

    const get = async <T>(key: string): Promise<T | null> => {
        const result = await redis.get(key)

        const parsedResult = jsonTryParse<T>(result);

        return parsedResult
    }

    const remove = async (keys: string[]): Promise<number> => {
        return await redis.unlink(keys)
    }

    const removeByPrefix = async (prefix: string): Promise<void> => {
        const keys: string[] = []

        for await (const key of redis.scanIterator({
            MATCH: `${prefix}*`
        })) {
            keys.push(key)
        }

        await redis.unlink(keys)
    }

    const executeWithCache = async <T>(
        key: string,
        action: Function,
        seconds?: number
    ): Promise<T> => {
        const itemFromCache = await get<T>(key) as T

        if (itemFromCache) {
            return itemFromCache
        }

        const serviceResult = await action() as T

        seconds ? await set(key, serviceResult, seconds) : await set(key, serviceResult)

        return serviceResult
    }

    return {
        get,
        set,
        remove,
        removeByPrefix,
        executeWithCache
    }
}

export const useRedisPubSub = async (config: RedisConfig): Promise<RedisClientPubSub> => {
    const subscriber = await createRedisClient(config);

    const publisher = subscriber.duplicate();

    await publisher.connect();

    const subscribe = async (channel: string, callback: (message: string) => void): Promise<void> => {
        if (channel.endsWith('*')) {
            await subscriber.pSubscribe(channel, (message) => {
                callback(message);
            });
        } else {
            await subscriber.subscribe(channel, (message) => {
                callback(message);
            });
        }
    }

    const unsubscribe = async (channel: string): Promise<void> => {
        if (channel.endsWith('*')) {
            await subscriber.pUnsubscribe(channel);
        } else {
            await subscriber.unsubscribe(channel);
        }
    }

    const publish = async <T>(channel: string, data: T): Promise<void> => {
        await publisher.publish(channel, JSON.stringify(data))
    }

    return {
        subscribe,
        unsubscribe,
        publish
    }
}
