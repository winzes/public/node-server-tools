import {
    S3,
    PutObjectCommandInput,
    DeleteObjectCommand,
    DeleteObjectCommandInput
} from "@aws-sdk/client-s3"
import { Upload } from "@aws-sdk/lib-storage";
import { ReadStream } from "fs";

export type S3DOSpacesOptions = {
    protocol: 'http' | 'https'
    endpoint: string,
    region?: string,
    key: string,
    secretKey: string,
    bucket: string
}

export type FileOptions = {
    contentType?: string,
    folderDir?: string,
}

export const useS3DOSpaces = ({
    endpoint,
    key,
    secretKey,
    region = "us-east-1",
    bucket,
    protocol,
}: S3DOSpacesOptions) => {
    const s3Client = new S3({
        endpoint: `${protocol}://${endpoint}`,
        region,
        credentials: {
            accessKeyId: key,
            secretAccessKey: secretKey
        }
    });

    const uploadFile = async (createReadStream: () => ReadStream, filename: string, { folderDir, contentType }: FileOptions = {}) => {
        const bucketParams: PutObjectCommandInput = {
            Bucket: bucket,
            Key: "",
            Body: "",
            ACL: 'public-read'
        };

        const fileStream = createReadStream();

        fileStream.on("error", (error: Error) => console.error(error));

        bucketParams.Body = fileStream;

        bucketParams.Key = filename;

        if (folderDir) {
            bucketParams.Key = `${folderDir}/${filename}`
        }

        if (contentType) {
            bucketParams.ContentType = contentType
        }

        try {
            const parallelUploads3 = new Upload({
                client: s3Client,
                params: bucketParams
            });

            // parallelUploads3.on("httpUploadProgress", (progress) => {
            //     console.log(progress);
            // });

            await parallelUploads3.done();

            return {
                fullPath: `${protocol}://${bucket}.${endpoint}/${bucketParams.Key}`,
                relPath: bucketParams.Key
            }
        } catch (e) {
            console.log(e);
        }
    }

    const deleteFile = async (relPath: string) => {
        const bucketParams: DeleteObjectCommandInput = { Bucket: bucket, Key: relPath };

        try {
            await s3Client.send(new DeleteObjectCommand(bucketParams));

            return true;
        } catch (err) {
            console.log("Error", err);
            return false;
        }
    }

    return {
        uploadFile,
        deleteFile
    }
}
