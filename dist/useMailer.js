"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useMailer = void 0;
const nodemailer_1 = __importDefault(require("nodemailer"));
const useMailer = (options) => {
    const transporter = nodemailer_1.default.createTransport(options);
    const sendMail = (options) => __awaiter(void 0, void 0, void 0, function* () {
        return new Promise((res, rej) => {
            transporter.sendMail(options, function (error, info) {
                if (error) {
                    rej(error);
                }
                else {
                    res(info);
                }
            });
        });
    });
    return {
        sendMail
    };
};
exports.useMailer = useMailer;
//# sourceMappingURL=useMailer.js.map