import { RedisConfig, RedisClientPubSub, RedisClient } from './useRedis.types';
export declare const useRedisStore: (config?: RedisConfig) => Promise<RedisClient>;
export declare const useRedisPubSub: (config: RedisConfig) => Promise<RedisClientPubSub>;
