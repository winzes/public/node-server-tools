"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.withClustering = void 0;
const cluster_1 = __importDefault(require("cluster"));
const os_1 = require("os");
const setupWorkerProcesses = (options = {}) => {
    const { maxInstancesCount } = options;
    // to read number of cores on system
    const numCores = maxInstancesCount || (0, os_1.cpus)().length;
    console.log('Master cluster setting up ' + numCores + ' workers');
    // iterate on number of cores need to be utilized by an application
    // current example will utilize all of them
    for (let i = 0; i < numCores; i++) {
        // creating workers and pushing reference in an array
        // these references can be used to receive messages from workers
        cluster_1.default.fork();
    }
    // process is clustered on a core and process id is assigned
    cluster_1.default.on('online', function (worker) {
        console.log('Worker ' + worker.process.pid + ' is listening');
    });
    // if any of the worker process dies then start a new one by simply forking another one
    cluster_1.default.on('exit', function (worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        console.log('Starting a new worker');
        cluster_1.default.fork();
    });
};
const withClustering = (startServerFunc, options) => {
    // if it is a master process then call setting up worker process
    if (cluster_1.default.isMaster) {
        setupWorkerProcesses(options);
    }
    else {
        // to setup server configurations and share port address for incoming requests
        startServerFunc();
    }
};
exports.withClustering = withClustering;
//# sourceMappingURL=useClustering.js.map