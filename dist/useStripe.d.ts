import Stripe from 'stripe';
import { PaymentIntentOptions, StripePaymentResult, StripeWebHookOptions } from './useStripe.types';
export declare const useStripe: (apiKey: string) => {
    instance: Stripe;
    createPaymentIntent: <T = any>(paymentOptions: PaymentIntentOptions<T>) => Promise<StripePaymentResult<T>>;
    getPaymentIntent: <T_1>(id: string) => Promise<StripePaymentResult<T_1>>;
    addWebHook: (options: StripeWebHookOptions) => void;
    createCheckoutSession: (options: Stripe.Checkout.SessionCreateParams) => Promise<Stripe.Response<Stripe.Checkout.Session>>;
};
