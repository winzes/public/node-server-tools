"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useS3DOSpaces = void 0;
const client_s3_1 = require("@aws-sdk/client-s3");
const lib_storage_1 = require("@aws-sdk/lib-storage");
const useS3DOSpaces = ({ endpoint, key, secretKey, region = "us-east-1", bucket, protocol, }) => {
    const s3Client = new client_s3_1.S3({
        endpoint: `${protocol}://${endpoint}`,
        region,
        credentials: {
            accessKeyId: key,
            secretAccessKey: secretKey
        }
    });
    const uploadFile = (createReadStream, filename, { folderDir, contentType } = {}) => __awaiter(void 0, void 0, void 0, function* () {
        const bucketParams = {
            Bucket: bucket,
            Key: "",
            Body: "",
            ACL: 'public-read'
        };
        const fileStream = createReadStream();
        fileStream.on("error", (error) => console.error(error));
        bucketParams.Body = fileStream;
        bucketParams.Key = filename;
        if (folderDir) {
            bucketParams.Key = `${folderDir}/${filename}`;
        }
        if (contentType) {
            bucketParams.ContentType = contentType;
        }
        try {
            const parallelUploads3 = new lib_storage_1.Upload({
                client: s3Client,
                params: bucketParams
            });
            // parallelUploads3.on("httpUploadProgress", (progress) => {
            //     console.log(progress);
            // });
            yield parallelUploads3.done();
            return {
                fullPath: `${protocol}://${bucket}.${endpoint}/${bucketParams.Key}`,
                relPath: bucketParams.Key
            };
        }
        catch (e) {
            console.log(e);
        }
    });
    const deleteFile = (relPath) => __awaiter(void 0, void 0, void 0, function* () {
        const bucketParams = { Bucket: bucket, Key: relPath };
        try {
            yield s3Client.send(new client_s3_1.DeleteObjectCommand(bucketParams));
            return true;
        }
        catch (err) {
            console.log("Error", err);
            return false;
        }
    });
    return {
        uploadFile,
        deleteFile
    };
};
exports.useS3DOSpaces = useS3DOSpaces;
//# sourceMappingURL=useS3.js.map