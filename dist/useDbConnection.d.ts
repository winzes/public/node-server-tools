import "reflect-metadata";
import { DataSource, DataSourceOptions } from "typeorm";
export declare const startDbConnection: (databaseConfig: DataSourceOptions) => Promise<DataSource>;
