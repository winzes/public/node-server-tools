import { ReadStream } from "fs";
export declare type S3DOSpacesOptions = {
    protocol: 'http' | 'https';
    endpoint: string;
    region?: string;
    key: string;
    secretKey: string;
    bucket: string;
};
export declare type FileOptions = {
    contentType?: string;
    folderDir?: string;
};
export declare const useS3DOSpaces: ({ endpoint, key, secretKey, region, bucket, protocol, }: S3DOSpacesOptions) => {
    uploadFile: (createReadStream: () => ReadStream, filename: string, { folderDir, contentType }?: FileOptions) => Promise<{
        fullPath: string;
        relPath: string;
    }>;
    deleteFile: (relPath: string) => Promise<boolean>;
};
