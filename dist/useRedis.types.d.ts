export declare type RedisConfig = Readonly<{
    host: string;
    port: number;
    dbIndex: number;
    password?: string;
    ipVersion?: 0 | 4 | 6;
}>;
export declare type RedisClient = Readonly<{
    set: <T>(key: string, value: T, seconds: number) => Promise<void>;
    get: <T>(key: string) => Promise<string | T>;
    remove: (keys: string[]) => Promise<number>;
    removeByPrefix: (prefix: string) => Promise<void>;
    executeWithCache: <T>(key: string, action: Function, seconds?: number) => Promise<T>;
}>;
export declare type RedisHandlerFunction = (channel: string, message: string) => void;
export declare type RedisClientPubSub = Readonly<{
    subscribe: (channel: string, callback: (message: string) => void) => Promise<void>;
    unsubscribe: (channel: string, callback: (message: string) => void) => Promise<void>;
    publish: <T>(channel: string, data: T) => Promise<void>;
}>;
