"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useRedisPubSub = exports.useRedisStore = void 0;
const redis_1 = require("redis");
const utils_1 = require("./utils");
const createRedisClient = (config) => __awaiter(void 0, void 0, void 0, function* () {
    const redis = (0, redis_1.createClient)({
        socket: {
            host: config.host,
            port: config.port,
            passphrase: config.password,
            family: config.ipVersion
        },
        database: config.dbIndex
    });
    redis.on('error', (err) => console.log('Redis Client Error', err));
    yield redis.connect();
    return redis;
});
const useRedisStore = (config) => __awaiter(void 0, void 0, void 0, function* () {
    if (!config) {
        return (0, utils_1.useLocalCache)();
    }
    const redis = yield createRedisClient(config);
    const set = (key, value, seconds) => __awaiter(void 0, void 0, void 0, function* () {
        if (seconds) {
            yield redis.setEx(key, seconds, JSON.stringify(value));
            return;
        }
        yield redis.set(key, JSON.stringify(value));
    });
    const get = (key) => __awaiter(void 0, void 0, void 0, function* () {
        const result = yield redis.get(key);
        const parsedResult = (0, utils_1.jsonTryParse)(result);
        return parsedResult;
    });
    const remove = (keys) => __awaiter(void 0, void 0, void 0, function* () {
        return yield redis.unlink(keys);
    });
    const removeByPrefix = (prefix) => __awaiter(void 0, void 0, void 0, function* () {
        var e_1, _a;
        const keys = [];
        try {
            for (var _b = __asyncValues(redis.scanIterator({
                MATCH: `${prefix}*`
            })), _c; _c = yield _b.next(), !_c.done;) {
                const key = _c.value;
                keys.push(key);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) yield _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        yield redis.unlink(keys);
    });
    const executeWithCache = (key, action, seconds) => __awaiter(void 0, void 0, void 0, function* () {
        const itemFromCache = yield get(key);
        if (itemFromCache) {
            return itemFromCache;
        }
        const serviceResult = yield action();
        seconds ? yield set(key, serviceResult, seconds) : yield set(key, serviceResult);
        return serviceResult;
    });
    return {
        get,
        set,
        remove,
        removeByPrefix,
        executeWithCache
    };
});
exports.useRedisStore = useRedisStore;
const useRedisPubSub = (config) => __awaiter(void 0, void 0, void 0, function* () {
    const subscriber = yield createRedisClient(config);
    const publisher = subscriber.duplicate();
    yield publisher.connect();
    const subscribe = (channel, callback) => __awaiter(void 0, void 0, void 0, function* () {
        if (channel.endsWith('*')) {
            yield subscriber.pSubscribe(channel, (message) => {
                callback(message);
            });
        }
        else {
            yield subscriber.subscribe(channel, (message) => {
                callback(message);
            });
        }
    });
    const unsubscribe = (channel) => __awaiter(void 0, void 0, void 0, function* () {
        if (channel.endsWith('*')) {
            yield subscriber.pUnsubscribe(channel);
        }
        else {
            yield subscriber.unsubscribe(channel);
        }
    });
    const publish = (channel, data) => __awaiter(void 0, void 0, void 0, function* () {
        yield publisher.publish(channel, JSON.stringify(data));
    });
    return {
        subscribe,
        unsubscribe,
        publish
    };
});
exports.useRedisPubSub = useRedisPubSub;
//# sourceMappingURL=useRedis.js.map