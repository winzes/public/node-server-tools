/// <reference types="node" />
import { IResolvers, MercuriusContext } from "mercurius";
import { DocumentNode, GraphQLResolveInfo } from "graphql";
import { GraphqlServerConfig } from "./types";
import { ReadStream } from "fs";
export declare type GraphQLContext = MercuriusContext;
export declare type GraphQLSubscriptionsContext = MercuriusContext & {
    _connectionInit: any;
};
export declare type GraphQLInfo = GraphQLResolveInfo;
export declare type TypeDefs = DocumentNode[];
export declare type Resolvers = IResolvers<any, any> | Array<IResolvers<any, any>>;
export declare type GraphQLFile = {
    createReadStream: () => ReadStream;
    filename: string;
};
export declare const startGQLServer: (typeDefs: TypeDefs, resolvers: Resolvers, config: GraphqlServerConfig) => Promise<void>;
