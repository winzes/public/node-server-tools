"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.rootTypeDefs = exports.rootResolvers = void 0;
var resolvers_1 = require("./resolvers");
Object.defineProperty(exports, "rootResolvers", { enumerable: true, get: function () { return __importDefault(resolvers_1).default; } });
var root_schema_1 = require("./root.schema");
Object.defineProperty(exports, "rootTypeDefs", { enumerable: true, get: function () { return __importDefault(root_schema_1).default; } });
//# sourceMappingURL=index.js.map