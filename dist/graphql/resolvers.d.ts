declare const _default: {
    Query: {
        _empty: () => any;
    };
    Mutation: {
        _empty: () => any;
    };
    Subscription: {
        _empty: () => any;
    };
    Upload: any;
};
export default _default;
