"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_tag_1 = __importDefault(require("graphql-tag"));
exports.default = (0, graphql_tag_1.default) `
    scalar Upload

    "Date in YYYY-MM-DD format"
    scalar Date

    "DateTime in RFC3339 (yyyy-MM-dd'T'HH:mm:ss'Z') format"
    scalar DateTime

    type Query {
        _empty: String
    }

    type Mutation {
        _empty: String
    }

    type Subscription {
        _empty: String
    }
`;
//# sourceMappingURL=root.schema.js.map