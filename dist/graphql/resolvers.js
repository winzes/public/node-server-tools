"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const GraphQLUpload_js_1 = __importDefault(require("graphql-upload/GraphQLUpload.js"));
exports.default = {
    Query: {
        _empty: () => null,
    },
    Mutation: {
        _empty: () => null,
    },
    Subscription: {
        _empty: () => null,
    },
    Upload: GraphQLUpload_js_1.default,
};
//# sourceMappingURL=resolvers.js.map