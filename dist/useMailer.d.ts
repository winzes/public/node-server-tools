import Mail from 'nodemailer/lib/mailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
export declare const useMailer: (options: SMTPTransport | SMTPTransport.Options) => {
    sendMail: (options: Mail.Options) => Promise<unknown>;
};
