import { ClusteringConfig } from './types';
export declare const withClustering: (startServerFunc: Function, options?: ClusteringConfig) => void;
