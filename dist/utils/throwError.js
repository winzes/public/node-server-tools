"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.throwCustomErrorWithStatusCode = exports.throwCustomError = exports.CustomError = void 0;
class CustomError extends Error {
    constructor(message, name) {
        super(message);
        this.name = name;
    }
}
exports.CustomError = CustomError;
const throwCustomError = (message, name = 'CustomError') => {
    throw new CustomError(message, name);
};
exports.throwCustomError = throwCustomError;
const throwCustomErrorWithStatusCode = (message, statusCode, name = 'CustomError') => {
    const error = {
        message,
        statusCode
    };
    throw new CustomError(JSON.stringify(error), name);
};
exports.throwCustomErrorWithStatusCode = throwCustomErrorWithStatusCode;
//# sourceMappingURL=throwError.js.map