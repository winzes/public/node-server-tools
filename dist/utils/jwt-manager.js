"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateToken = exports.generateToken = void 0;
const jsonwebtoken_1 = __importStar(require("jsonwebtoken"));
const defaultOptions = {
    tokenSecret: 'default_secret',
    expiteInMinutes: 60,
    refreshExpireInMinutes: 60
};
const generateToken = (data, options = defaultOptions) => {
    const { expiteInMinutes, tokenSecret } = options;
    const token = jsonwebtoken_1.default.sign({
        data
    }, tokenSecret, { expiresIn: expiteInMinutes * 60 });
    const timestamp = new Date().getTime() + expiteInMinutes * 60 * 1000;
    return {
        token,
        expireDate: new Date(timestamp).toISOString()
    };
};
exports.generateToken = generateToken;
const validateToken = (token, options = defaultOptions, tokenExpireCallback) => {
    const { tokenSecret, refreshExpireInMinutes } = options;
    try {
        const decoded = jsonwebtoken_1.default.verify(token, tokenSecret);
        return decoded.data;
    }
    catch (error) {
        if (tokenExpireCallback && error instanceof jsonwebtoken_1.TokenExpiredError) {
            const { data, exp } = jsonwebtoken_1.default.verify(token, tokenSecret, { ignoreExpiration: true });
            const isRefreshTokenExpired = !!refreshExpireInMinutes &&
                (Date.now() / 1000) > exp + (refreshExpireInMinutes * 60);
            return tokenExpireCallback(data, exp, isRefreshTokenExpired);
        }
        return null;
    }
};
exports.validateToken = validateToken;
//# sourceMappingURL=jwt-manager.js.map