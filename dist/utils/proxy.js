"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useProxy = void 0;
const useProxy = (obj, beforeFunctionCallback, afterFunctionCallback) => {
    const handler = {
        get(target, propKey, receiver) {
            const targetValue = Reflect.get(target, propKey, receiver);
            if (typeof targetValue === 'function') {
                return (...args) => {
                    const functionName = propKey.toString();
                    beforeFunctionCallback(functionName, args);
                    const result = targetValue.apply(this, args);
                    afterFunctionCallback(functionName, args, result);
                    return result;
                };
            }
            else {
                return targetValue;
            }
        }
    };
    return new Proxy(obj, handler);
};
exports.useProxy = useProxy;
//# sourceMappingURL=proxy.js.map