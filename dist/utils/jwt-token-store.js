"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeAllTokensForUser = exports.removeTokenForUser = exports.isValidTokenInStore = exports.setTokenToStore = void 0;
const TOKEN_STORE = new Map();
const setTokenToStore = (userId, token) => {
    var _a;
    ((_a = TOKEN_STORE.get(userId)) === null || _a === void 0 ? void 0 : _a.push(token)) || TOKEN_STORE.set(userId, [token]);
};
exports.setTokenToStore = setTokenToStore;
const isValidTokenInStore = (userId, token) => { var _a; return (_a = TOKEN_STORE.get(userId)) === null || _a === void 0 ? void 0 : _a.some(userToken => userToken === token); };
exports.isValidTokenInStore = isValidTokenInStore;
const removeTokenForUser = (userId, token) => {
    const userTokens = TOKEN_STORE.get(userId);
    if (!userTokens) {
        return;
    }
    userTokens.splice(userTokens.indexOf(token), 1);
    if (!userTokens.length) {
        TOKEN_STORE.delete(userId);
    }
};
exports.removeTokenForUser = removeTokenForUser;
const removeAllTokensForUser = (userId) => {
    TOKEN_STORE.delete(userId);
};
exports.removeAllTokensForUser = removeAllTokensForUser;
//# sourceMappingURL=jwt-token-store.js.map