/// <reference types="node" />
import EventEmitter from "events";
import { GraphQLResolveInfo } from "graphql";
export declare const getHeaderValueFromContext: (context: any, headerName: string) => string;
export declare const getQueryFields: (info: GraphQLResolveInfo) => string[];
export declare class CustomPubSub {
    emitter: EventEmitter;
    constructor();
    subscribe(topic: string, queue: any): Promise<void>;
    publish<TResult = any>(event: {
        topic: string;
        payload: TResult;
    }, callback: () => void): void;
}
