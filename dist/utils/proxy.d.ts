declare type BeforeFunctionCallback = <T>(funtionName: string, args: T) => void;
declare type AfterFunctionCallback = <T, TT>(funtionName: string, args: T, result: TT) => void;
export declare const useProxy: <T extends object>(obj: T, beforeFunctionCallback: BeforeFunctionCallback, afterFunctionCallback: AfterFunctionCallback) => object;
export {};
