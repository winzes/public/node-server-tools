"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomPubSub = exports.getQueryFields = exports.getHeaderValueFromContext = void 0;
const events_1 = __importDefault(require("events"));
const getHeaderValueFromContext = (context, headerName) => {
    var _a, _b;
    return (_b = (_a = context === null || context === void 0 ? void 0 : context.reply) === null || _a === void 0 ? void 0 : _a.request) === null || _b === void 0 ? void 0 : _b.headers[headerName];
};
exports.getHeaderValueFromContext = getHeaderValueFromContext;
const getSelectionsRecursive = (selectionSet, selectionsOutput, parentPropName) => {
    const selections = selectionSet.selections;
    selections.map(({ name, selectionSet }) => {
        const fieldName = name.value;
        if (fieldName === '__typename') {
            return;
        }
        if (!selectionSet && fieldName) {
            const generateName = parentPropName ? `${parentPropName}.${fieldName}` : fieldName;
            selectionsOutput.push(generateName);
            return;
        }
        getSelectionsRecursive(selectionSet, selectionsOutput, parentPropName ? `${parentPropName}.${fieldName}` : fieldName);
    });
};
const getQueryFields = (info) => {
    var _a;
    const select = [];
    const fieldNodes = (_a = info === null || info === void 0 ? void 0 : info.fieldNodes) === null || _a === void 0 ? void 0 : _a[0];
    const selectionSet = fieldNodes && fieldNodes.selectionSet;
    selectionSet && getSelectionsRecursive(selectionSet, select);
    return select;
};
exports.getQueryFields = getQueryFields;
class CustomPubSub {
    constructor() {
        this.emitter = new events_1.default();
    }
    subscribe(topic, queue) {
        return __awaiter(this, void 0, void 0, function* () {
            const listener = (value) => {
                queue.push(value);
            };
            const close = () => {
                this.emitter.removeListener(topic, listener);
            };
            this.emitter.on(topic, listener);
            queue.close = close;
        });
    }
    publish(event, callback) {
        this.emitter.emit(event.topic, event.payload);
        callback();
    }
}
exports.CustomPubSub = CustomPubSub;
//# sourceMappingURL=graphql-utils.js.map