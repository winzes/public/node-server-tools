export declare const setTokenToStore: (userId: number, token: string) => void;
export declare const isValidTokenInStore: (userId: number, token: string) => boolean;
export declare const removeTokenForUser: (userId: number, token: string) => void;
export declare const removeAllTokensForUser: (userId: number) => void;
