export declare const getObjectHash: (obj: object) => string;
export declare const jsonTryParse: <T>(jsonString: string) => T;
export declare const jsonTryParseOrString: <T>(jsonString: string) => string | T;
