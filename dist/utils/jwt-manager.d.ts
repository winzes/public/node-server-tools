import { JwtToken, TokenExpireFunction, TokenOptions } from './jwt-manager.types';
export declare const generateToken: <T>(data: T, options?: TokenOptions) => JwtToken;
export declare const validateToken: <T>(token: string, options?: TokenOptions, tokenExpireCallback?: TokenExpireFunction<T>) => T;
