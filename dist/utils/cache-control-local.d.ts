export declare const useLocalCache: () => {
    get: <T>(key: string) => Promise<T>;
    set: <T_1>(key: string, item: T_1, seconds?: number) => Promise<void>;
    remove: (keys: string[]) => Promise<number>;
    removeByPrefix: (startWithKey: string) => Promise<void>;
    executeWithCache: <T_2>(key: string, action: Function, seconds?: number) => Promise<T_2>;
};
