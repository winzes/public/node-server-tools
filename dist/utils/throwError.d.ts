export declare class CustomError extends Error {
    constructor(message: string, name: string);
}
export declare const throwCustomError: (message: string, name?: string) => never;
export declare const throwCustomErrorWithStatusCode: (message: string, statusCode: number, name?: string) => never;
