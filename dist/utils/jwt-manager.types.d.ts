export declare type TokenOptions = Readonly<{
    tokenSecret: string;
    expiteInMinutes?: number;
    refreshExpireInMinutes?: number;
}>;
export declare type TokenExpireFunction<T> = (data: T, exp: number, isRefreshTokenExpired: boolean) => T;
export declare type Decoded<T> = Readonly<{
    data: T;
    exp: number;
}>;
export declare type JwtToken = Readonly<{
    token: string;
    expireDate: string;
}>;
