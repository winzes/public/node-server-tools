"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useLocalCache = void 0;
const useLocalCache = () => {
    const inMemoryCache = {};
    const set = (key, item, seconds) => __awaiter(void 0, void 0, void 0, function* () {
        const cacheItem = {
            item,
            expiry: seconds ? new Date().getTime() + (seconds * 1000) : undefined
        };
        inMemoryCache[key] = cacheItem;
    });
    const get = (key) => __awaiter(void 0, void 0, void 0, function* () {
        const cacheItem = inMemoryCache[key];
        if (!cacheItem) {
            return null;
        }
        if (cacheItem.expiry && new Date().getTime() > cacheItem.expiry) {
            inMemoryCache[key] = undefined;
            return null;
        }
        return cacheItem.item;
    });
    const remove = (keys) => __awaiter(void 0, void 0, void 0, function* () {
        keys.forEach(key => {
            inMemoryCache[key] = undefined;
        });
        return 1;
    });
    const removeByPrefix = (startWithKey) => __awaiter(void 0, void 0, void 0, function* () {
        const keysToRemove = Object.keys(inMemoryCache).filter(key => key.startsWith(startWithKey));
        remove(keysToRemove);
    });
    const executeWithCache = (key, action, seconds) => __awaiter(void 0, void 0, void 0, function* () {
        const itemFromCache = yield get(key);
        if (itemFromCache) {
            return itemFromCache;
        }
        const serviceResult = yield action();
        yield set(key, serviceResult, seconds);
        return serviceResult;
    });
    return {
        get,
        set,
        remove,
        removeByPrefix,
        executeWithCache
    };
};
exports.useLocalCache = useLocalCache;
//# sourceMappingURL=cache-control-local.js.map