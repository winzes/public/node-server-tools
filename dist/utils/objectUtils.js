"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jsonTryParseOrString = exports.jsonTryParse = exports.getObjectHash = void 0;
const object_hash_1 = __importDefault(require("object-hash"));
const getObjectHash = (obj) => {
    return (0, object_hash_1.default)(obj);
};
exports.getObjectHash = getObjectHash;
const jsonTryParse = (jsonString) => {
    try {
        return JSON.parse(jsonString);
    }
    catch (_a) {
        return null;
    }
};
exports.jsonTryParse = jsonTryParse;
const jsonTryParseOrString = (jsonString) => {
    try {
        return JSON.parse(jsonString);
    }
    catch (_a) {
        return jsonString;
    }
};
exports.jsonTryParseOrString = jsonTryParseOrString;
//# sourceMappingURL=objectUtils.js.map