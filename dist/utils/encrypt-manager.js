"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.encrypt = void 0;
const crypto_1 = __importDefault(require("crypto"));
const encrypt = (dataString) => {
    return crypto_1.default.createHash('sha256')
        .update(dataString)
        .digest('base64');
};
exports.encrypt = encrypt;
//# sourceMappingURL=encrypt-manager.js.map