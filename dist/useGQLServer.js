"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.startGQLServer = void 0;
const fastify_1 = __importDefault(require("fastify"));
const mercurius_1 = __importDefault(require("mercurius"));
const mercurius_upload_1 = __importDefault(require("mercurius-upload"));
const altair_fastify_plugin_1 = __importDefault(require("altair-fastify-plugin"));
const cors_1 = __importDefault(require("@fastify/cors"));
const static_1 = __importDefault(require("@fastify/static"));
const websocket_1 = __importDefault(require("@fastify/websocket"));
const mqemitter_redis_1 = __importDefault(require("mqemitter-redis"));
const utils_1 = require("./utils");
const DEFAULT_VALUES = {
    PLAYGROUND_URL: "/playground",
    GQL_PATH: "/graphql",
    WITH_ALTAIR: true,
};
const addGraphQLServer = (app, typeDefs, resolvers, config) => __awaiter(void 0, void 0, void 0, function* () {
    const { useAltairPlayground = DEFAULT_VALUES.WITH_ALTAIR, playgroundRoute = DEFAULT_VALUES.PLAYGROUND_URL, graphqlRoute = DEFAULT_VALUES.GQL_PATH, disablePlayground, graphqlSubscriptions, contextBuilder, } = config;
    app.register(websocket_1.default, {
        options: {
            maxPayload: 1048576,
        },
    });
    app.register(mercurius_upload_1.default);
    const { makeExecutableSchema } = yield Promise.resolve().then(() => __importStar(require("@graphql-tools/schema")));
    const schema = makeExecutableSchema({
        typeDefs,
        resolvers,
        resolverValidationOptions: {
            requireResolversForArgs: "error",
        },
    });
    let emitter = undefined;
    if (graphqlSubscriptions === null || graphqlSubscriptions === void 0 ? void 0 : graphqlSubscriptions.redisConfig) {
        emitter = (0, mqemitter_redis_1.default)({
            port: graphqlSubscriptions.redisConfig.port,
            host: graphqlSubscriptions.redisConfig.host,
            username: graphqlSubscriptions.redisConfig.username,
            password: graphqlSubscriptions.redisConfig.password,
            connectionString: graphqlSubscriptions.redisConfig.connectionString,
        });
    }
    app.addContentTypeParser("application/json", { parseAs: "string" }, (req, body, done) => {
        if (req.url.includes("stripe")) {
            return done(null, body);
        }
        return done(null, (0, utils_1.jsonTryParseOrString)(body));
    });
    app.register(mercurius_1.default, {
        schema,
        jit: 1,
        graphiql: disablePlayground
            ? false
            : useAltairPlayground
                ? false
                : "graphiql",
        ide: false,
        path: graphqlRoute,
        context: contextBuilder
            ? (request, reply) => __awaiter(void 0, void 0, void 0, function* () {
                return yield contextBuilder(request, reply);
            })
            : undefined,
        errorFormatter: (err, ctx) => {
            var _a;
            const response = mercurius_1.default.defaultErrorFormatter(err, ctx);
            const error = (_a = err.errors) === null || _a === void 0 ? void 0 : _a[0];
            if (error && error.message.includes("statusCode")) {
                const errorObj = JSON.parse(error.message);
                response.statusCode = errorObj.statusCode || 500;
                response.response.errors[0].message =
                    errorObj.message || error.message;
            }
            return response;
        },
        subscription: graphqlSubscriptions && {
            emitter,
            onConnect: graphqlSubscriptions.onConnect
                ? ({ type, payload }) => {
                    return {
                        payload,
                    };
                }
                : undefined,
            context: graphqlSubscriptions.contextBuilder
                ? (_connection, request) => __awaiter(void 0, void 0, void 0, function* () {
                    const { contextBuilder } = graphqlSubscriptions;
                    return yield contextBuilder(request);
                })
                : undefined,
            verifyClient: graphqlSubscriptions.verifyClient
                ? ({ req }, next) => __awaiter(void 0, void 0, void 0, function* () {
                    const { verifyClient } = graphqlSubscriptions;
                    const isVerified = yield verifyClient(req);
                    next(isVerified);
                })
                : undefined,
        },
    });
    useAltairPlayground &&
        !disablePlayground &&
        app.register(altair_fastify_plugin_1.default, {
            path: playgroundRoute,
            baseURL: `${playgroundRoute}/`,
            endpointURL: graphqlRoute,
        });
});
const startGQLServer = (typeDefs, resolvers, config) => __awaiter(void 0, void 0, void 0, function* () {
    const { host, port, playgroundRoute = DEFAULT_VALUES.PLAYGROUND_URL, graphqlRoute = DEFAULT_VALUES.GQL_PATH, staticFolderPath, staticRoutePrefix, disablePlayground, injectCustomRoutes, } = config;
    const app = (0, fastify_1.default)();
    app.register(cors_1.default);
    yield addGraphQLServer(app, typeDefs, resolvers, config);
    staticFolderPath &&
        app.register(static_1.default, {
            root: staticFolderPath,
            prefix: staticRoutePrefix,
            decorateReply: false,
        });
    injectCustomRoutes === null || injectCustomRoutes === void 0 ? void 0 : injectCustomRoutes(app);
    try {
        yield app.listen({ host: "0.0.0.0", port });
        console.log(`GraphQL Server Available at http://${host}:${port}${graphqlRoute}`);
        !disablePlayground &&
            console.log(`Playground UI Available at http://${host}:${port}${playgroundRoute}`);
    }
    catch (error) {
        console.error("Error starting GQL Server: ", error.message);
    }
});
exports.startGQLServer = startGQLServer;
//# sourceMappingURL=useGQLServer.js.map