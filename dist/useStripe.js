"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useStripe = void 0;
const stripe_1 = __importDefault(require("stripe"));
const fastify_1 = __importDefault(require("fastify"));
const cors_1 = __importDefault(require("@fastify/cors"));
const useStripe = (apiKey) => {
    const stripe = new stripe_1.default(apiKey, {
        apiVersion: '2022-08-01'
    });
    const createPaymentIntent = (paymentOptions) => __awaiter(void 0, void 0, void 0, function* () {
        return yield stripe.paymentIntents.create(paymentOptions);
    });
    const getPaymentIntent = (id) => __awaiter(void 0, void 0, void 0, function* () {
        return yield stripe.paymentIntents.capture(id);
    });
    const addWebHook = (options) => {
        const { port, route, secret, handler } = options;
        const webHookRoute = route || `/stripe/webhook`;
        const server = (0, fastify_1.default)();
        server.register(cors_1.default);
        server.addContentTypeParser("application/json", { parseAs: "string" }, (req, body, done) => done(null, body));
        server.post(webHookRoute, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
            const signature = req.headers['stripe-signature'];
            try {
                const event = stripe.webhooks.constructEvent(req.body, signature, secret);
                const data = (event.data || req.body.data);
                const eventType = event.type || req.body.type;
                yield handler(data, eventType);
            }
            catch (err) {
                console.log(err.message);
                return res.status(400).send();
            }
            res.status(200).send();
        }));
        server.listen({ port, host: '0.0.0.0' }, (err) => {
            if (err)
                throw err;
            console.log(`Stripe WebHook listen on http://127.0.0.1:${port}${webHookRoute}`);
        });
    };
    const createCheckoutSession = (options) => __awaiter(void 0, void 0, void 0, function* () {
        return yield stripe.checkout.sessions.create(options);
    });
    return {
        instance: stripe,
        createPaymentIntent,
        getPaymentIntent,
        addWebHook,
        createCheckoutSession
    };
};
exports.useStripe = useStripe;
//# sourceMappingURL=useStripe.js.map