import { throwCustomError } from '../../src'

import fs from 'fs'
import path from 'path'
import util from 'util'
import stream from 'stream'

const pipeline = util.promisify(stream.pipeline)
const uploadsDir = path.resolve(process.cwd(), 'uploads')

if (!fs.existsSync(uploadsDir)) {
    fs.mkdirSync(uploadsDir);
}

export type UploadOptions = {
    folder?: string;
}

export const uploadFile = async (
    filename: string,
    readStream: NodeJS.ReadableStream,
    options: UploadOptions = {}
): Promise<string> => {
    const { folder } = options

    const uploadFolder = folder ? path.join(uploadsDir, folder) : uploadsDir;

    if (!fs.existsSync(uploadFolder)) {
        fs.mkdirSync(uploadFolder);
    }

    const filePath = path.join(uploadFolder, filename || '')
    const ws = fs.createWriteStream(filePath)

    try {
        await pipeline(readStream, ws)
    } catch (e) {
        !filename && throwCustomError("File is missing!")
    }

    return filePath
}
