import {
  // withClustering, 
  startGQLServer,
  useStripe,
  StripeEventData
} from '../src'
import path from 'path'

import { schemas, resolvers } from './graphql'
import { createClient, SubscribePayload } from 'graphql-ws'
import ws from 'ws'

const startTestSubscriptions = async () => {
  const stripe = useStripe(process.env.STRIPE_KEY as string)

  type StripeMetadata = {
    username: string
  }

  await startGQLServer(schemas, resolvers, {
    host: '127.0.0.1',
    port: 3035,
    useAltairPlayground: true,
    staticFolderPath: path.join(process.cwd(), 'uploads'),
    staticRoutePrefix: '/cdn/',
    graphqlSubscriptions: {},
    injectCustomRoutes: (server) => {
      server.post('/stripe/charge', async (req, res) => {
        const { currency, id } = req.body as any;

        try {
          await stripe.createPaymentIntent<StripeMetadata>({
            amount: 1 * 100,
            currency: currency,
            payment_method: id,
            metadata: { username: 'test' },
            confirm: true,
          })

          res.statusCode = 200
          res.send({
            message: "Payment Successful",
            success: true,
          })
        } catch (e) {
          res.statusCode = 400
          console.log(e.message)
          res.send({
            message: "Payment Failed",
            success: false,
          })
        }
      })

      server.get('/stripe-session', async (res, req) => {
        const domain = 'http://127.0.0.1:3035'

        const stripeSession = await stripe.createCheckoutSession({
          line_items: [
            {
              // Provide the exact Price ID (e.g. pr_1234) of the product you want to sell:
              // price: 'price_1JpqKZC3BBAU2TYaZqHDX5cy',
              // quantity: 1,

              // Or Provider dynamic payment data product:
              price_data: {
                currency: 'usd',
                unit_amount: 1 * 100,
                product_data: {
                  name: 'TEST_VIP4E'
                }
              },
              quantity: 1,
            },
          ],
          payment_intent_data: {
            metadata: { username: 'alo_balo' }
          },
          // subscription_data: {
          //   metadata: { username: 'alo_balo' }
          // },
          payment_method_types: [
            'card',
          ],
          mode: 'payment',
          success_url: `${domain}/success.html`,
          cancel_url: `${domain}/cancel.html`,
        })

        req.send(stripeSession.url)
      })
    }
  })

  const handler = (data: StripeEventData<StripeMetadata>, eventType: string) => {
    if (eventType === 'payment_intent.succeeded') {
      console.log('💰 Payment captured!', data.object.metadata);
    } else if (eventType === 'payment_intent.payment_failed') {
      console.log('❌ Payment failed.');
    }
  }

  stripe.addWebHook({
    port: 3036,
    secret: process.env.STRIPE_WEBHOOK_SECRET as string,
    handler
  })

  const client = createClient({
    url: 'ws://127.0.0.1:3035/graphql',
    webSocketImpl: ws,
  });

  const subscribe = <T>(payload: SubscribePayload) =>
    client.subscribe<T>(payload, {
      next: (data) => console.log('SUCCESS:', data),
      error: (error) => console.error('ERROR:', error),
      complete: () => { },
    });

  // use
  const unsubscribe = subscribe({
    query: `subscription notification {
      notificationAdded{
        message
      }
    }`,
  });

  // setTimeout(() => {
  //   unsubscribe();
  // }, 20000);
}

// withClustering(start, { MAX_CLUSTERS_COUNT: 1 })

startTestSubscriptions()
