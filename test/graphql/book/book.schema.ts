import { gql } from "../../../src";

export default gql`
    type Book {
        name: String!
        price: Int
        author: Author!

    }

    type Author {
        firstName: String!
        lastName: String!
        country: Country!
    }

    type Country {
        name: String!
    }

    type Notification {
        id: ID!
        message: String
    }

    extend type Query {
        getBook: Book

        notifications: [Notification!]!
    }

    extend type Mutation {
        updateBookName(payload: String): Book!

        uploadImage(payload: Upload): String

        deleteImage(payload: String): Boolean

        addNotification(payload: String): Notification!
    }

    extend type Subscription {
        notificationAdded: Notification
    }
`;