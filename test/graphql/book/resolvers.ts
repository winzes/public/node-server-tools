import { GraphQLResolveInfo } from 'graphql'
import { CustomInput } from '../types'

import { bookService } from '../../services'
import { Book } from "../../services/book";
import { getQueryFields, GraphQLContext, GraphQLFile, useS3DOSpaces } from '../../../src';

let idCount = 1
const notifications = [
  {
    id: idCount,
    message: 'Notification message'
  }
]

const NOTIFICATION_TOPIC = 'NOTIFICATION_ADDED'

const space = useS3DOSpaces({
  protocol: 'https',
  endpoint: 'fra1.digitaloceanspaces.com',
  key: process.env.S3_KEY as string,
  secretKey: process.env.S3_SECRET as string,
  bucket: 'winzes'
})

export default {
  Query: {
    getBook: async (_: Object, __: Object, context: GraphQLContext, info: GraphQLResolveInfo): Promise<Book> => {
      console.log(getQueryFields(info));
      return await bookService.getBook()
    },
    notifications: () => notifications
  },
  Mutation: {
    updateBookName: async (_: Object, { payload }: CustomInput<string>): Promise<Book> => {
      const book = await bookService.getBook()
      book.name = payload
      return book
    },
    uploadImage: async (_: Object, { payload }: CustomInput<GraphQLFile>) => {
      const { createReadStream, filename } = await payload

      const filenameSplit = filename.split('.')
      const fileExtension = filenameSplit[filenameSplit.length - 1]

      const result = await space.uploadFile(createReadStream, filename, {
        contentType: `image/${fileExtension}`,
        folderDir: 'image/test'
      })

      return result?.fullPath

      // return await uploadFile(filename, createReadStream(), {
      //   folder: 'book'
      // })
    },
    deleteImage: async (_: Object, { payload }: CustomInput<string>) => {
      return await space.deleteFile(payload)
    },
    addNotification: async (_: Object, { payload }: CustomInput<string>, { pubsub }: GraphQLContext) => {
      const id = idCount++
      const notification = {
        id,
        message: payload
      }

      notifications.push(notification)

      await pubsub.publish({
        topic: NOTIFICATION_TOPIC,
        payload: {
          notificationAdded: notification
        }
      })

      return notification
    }
  },
  Subscription: {
    notificationAdded: {
      subscribe: async (root: Object, args: Object, { pubsub }: GraphQLContext) =>
        await pubsub.subscribe(NOTIFICATION_TOPIC)
    }
  }
}
