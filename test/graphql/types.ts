export type CustomContext = {
    userData: any
    userToken: string
}

export interface CustomInput<T> {
    readonly payload: T;
}