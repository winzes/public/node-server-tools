import { rootResolvers } from '../../src'
import { bookResolvers } from './book'

const resolvers = [
    rootResolvers,
    bookResolvers,
]

export default resolvers