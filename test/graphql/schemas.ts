import { rootTypeDefs } from '../../src'
import { bookTypeDefs } from './book'

const typeDefs = [
    rootTypeDefs,
    bookTypeDefs,
];

export default typeDefs