import {
    useRedisPubSub,
    useRedisStore,
} from '../src'

export const createRedis = async () => {
    const redis = await useRedisStore({
        port: 6379,
        host: "127.0.0.1",
        dbIndex: 0,
    })

    const KEY = 'test';

    await redis.set(KEY, 'some test string to cache', 300);

    const getValue = await redis.get<string>(KEY);
    console.log("Value from cache: ", getValue)

    await redis.remove([KEY])
}

export const createRedisSub = async () => {
    const redis = await useRedisPubSub({
        port: 6379,
        host: "127.0.0.1",
        dbIndex: 0,
    })

    await redis.subscribe('test-channel', (message) => {
        console.log(`Message from test-channel - ${message}`)
    });

    await redis.publish<string>('test-channel', 'Hello!')
}
