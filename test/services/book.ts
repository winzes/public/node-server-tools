export type Author = {
    firstName: string;
    lastName: string;
    country: Country;
}

export type Book = {
    name: string;
    price?: number;
    author: Author;
};

export type Country = {
    name: string;
}

const book: Book = {
    name: "Book of Books",
    price: 5,
    author: {
        firstName: 'John',
        lastName: 'John',
        country: {
            name: 'Bulgaria'
        }
    }
}

export const getBook = async (): Promise<Book> => {
    return await Promise.resolve(book)
}

export const updateBook = async (name: string): Promise<Book> => {
    book.name = name
    return await Promise.resolve(book)
}